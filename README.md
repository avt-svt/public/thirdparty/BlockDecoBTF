# BlockDecoBTF

Compute the block upper triangular form (BTF) of sparse square matrices. The BTF code is taken from  KLU/SuiteSparse of Tim Davis. The upstream URL is http://faculty.cse.tamu.edu/davis/suitesparse.html.
 
